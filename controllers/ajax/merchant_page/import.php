<?php

namespace Concrete\Package\CsvImporter\Controller\Ajax\MerchantPage;

use Core;
use Page;
use Block;
use Loader;
use \Concrete\Core\Page\Type\Type as CollectionType;
use Concrete\Core\Controller\Controller;
use \Concrete\Core\Http\Request;
use \Concrete\Core\Page\PageList;
use \Import\Page\Handler as PageHandler;

class Import extends Controller 
{
  protected $viewPath = '/ajax/merchant_page/page_updated';
  protected $merchMapCategories = array(
    'Antiques & Art Galleries'  => 'Shopping',
    'Apparel'                   => 'Shopping',
    'Books, Cards & Stationery' => 'Shopping',
    'Churches'                  => 'Attractions',
    'Crafts, Toys & Hobbies'    => 'Shopping',
    'Florists'                  => 'Shopping',
    'Food Specialties'          => 'Shopping',
    'Gifts & Collectables'      => 'Shopping',
    'Home Furnishings'          => 'Shopping',
    'Jewelry'                   => 'Shopping',
    'Lodging'                   => 'Businesses',
    'Media'                     => 'Businesses',
    'Medical & Pharmacies'      => 'Shopping',
    'Music'                     => 'Shopping',
    'Printers'                  => 'Shopping',
    'Restaurants'               => 'Dining',
    'Shoes & Accessories'       => 'Shopping',
    'Spas, Salons & Fitness'    => 'Businesses',
    'Specialty Shops'           => 'Shopping',
    'Sporting Goods'            => 'Shopping',
    'Wedding'                   => 'Shopping',
    'Local Government & Non-Profit Agencies' => 'Businesses',
    'Financial Services'        => 'Businesses',
    'Legal Services'            => 'Businesses',
    'Business Services'         => 'Businesses',
    'Theater/Attraction'        => 'Attractions',
  );

  public function update()
  {
    $r = Request::getInstance();
    $result = $this->import($r->request());
    $this->set('name', $r->get('name'));
    $this->set('action', 'updated');
    $this->set('result', $result);
    $this->view();
  }

  public function create() 
  {
    $r = Request::getInstance();
    $result = $this->import($r->request());
    $this->set('name', $r->get('name'));
    $this->set('action', 'created');
    $this->set('result', $result);
    $this->view();
  }

  public function view()
  {

  }

  private function import($params) 
  {
    // Check pageData
    $pageData = array(
      'name'                      => trim($params['name']),
      'cDescription'              => trim($params['cDescription']),
      'meta_keywords'             => isset($params['meta_keywords']) ? $params['meta_keywords'] : '' ,
      'merchant_address'          => isset($params['merchant_address']) ? $params['merchant_address'] : '',
      'merchant_phone'            => isset($params['merchant_phone']) ? $params['merchant_phone'] : '',
      'merchant_website'          => isset($params['merchant_website']) ? $params['merchant_website'] : '',
      'merchant_category'         => isset($params['merchant_category']) ? $params['merchant_category'] : '',
      'merchant_dtca_member'      => isset($params['merchant_dtca_member']) ? $params['merchant_dtca_member'] : 'No',
      'merchant_district'         => isset($params['merchant_district']) ? $params['merchant_district'] : '',
      'merchant_instagram_post_id'=> isset($params['merchant_instagram_post_id']) ? $params['merchant_instagram_post_id'] : '',
      'merchant_google_places_id' => isset($params['merchant_google_places_id']) ? $params['merchant_google_places_id'] : '',
      'merchant_facebook'         => isset($params['merchant_facebook']) ? $params['merchant_facebook'] : '',
      'merchant_twitter'          => isset($params['merchant_twitter']) ? $params['merchant_twitter'] : '',
      'merchant_instagram'        => isset($params['merchant_instagram']) ? $params['merchant_instagram'] : '',
    );
    $input_errors = array();
    if ($pageData['name'] == '') {
      array_push($input_errors, 'Name cannot be null');
    }
    if ($pageData['merchant_category'] == '') {
      array_push($input_errors, 'Missing category.');
    }
    elseif (!array_key_exists($pageData['merchant_category'], $this->merchMapCategories)) {
      array_push($input_errors, 'Invalid category: '.$pageData['merchant_category'].'. Make sure category exists in merchant_category attribute options.');
    }
    if ($pageData['merchant_address'] == '') {
      array_push($input_errors, 'Address is missing.');
    }
    if (!empty($input_errors))
      return array('success' => false, 'errors' => $input_errors);

    // Get page type and parent
    $pageType = CollectionType::getByHandle('merchant_page');
    $parentPage = Page::getByPath('/merchants');

    // Look for an existing page with this name
    $list = new \Concrete\Core\Page\PageList();
    $list->ignorePermissions();
    $list->filterByName($pageData['name'], true);
    $pl_results = $list->getResults();
    if ($list->getTotalResults() > 1) {
      return array('success' => false, 'errors' => array('Skipped page: '.$pageData['name'].'. Multiple pages with this name where found.'));
    }
    else if ($list->getTotalResults() == 1) {
      $newPage = $pl_results[0]; 
    }

    // Create or update page
    if (is_object($newPage)) {
      $newPage->update(array(
        'cDescription' => $pageData['cDescription'],
        'cDatePublic' => date('Y-m-d H:i:s')
      ));
    }
    else {
      $newPage = $parentPage->add($pageType, array(
        'name' => $pageData['name'],
        'cDescription' => $pageData['cDescription'],
        'cDatePublic' => date('Y-m-d H:i:s')
      ));
    }

    // Set attributes
    $newPage->setAttribute('exclude_nav', true);
    foreach ($pageData as $attr_handle => $value) {
      if ($attr_handle == 'name' || $attr_handle == 'cDescription' || $value == '')
        continue;
      $newPage->setAttribute($attr_handle, $value);
    }
    if (isset($this->merchMapCategories[$pageData['merchant_category']])) {
      $newPage->setAttribute('merchant_map_category', $this->merchMapCategories[$pageData['merchant_category']]);
    }

    // Call web services
    $handler = new PageHandler();
    if ($pageData['merchant_google_places_id'])
      $handler->fetch_google_places_data($newPage);
    if ($pageData['merchant_instagram_post_id'])
      $handler->fetch_ig_post($newPage);
    
    return array('success' => true, 'page' => $newPage);
  }
}