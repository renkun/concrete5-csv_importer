<?php

namespace Concrete\Package\CsvImporter\Controller\SinglePage\Dashboard;
use Core;
use Config;
use File;
use Page;
use PageList;
use \Concrete\Core\Page\Controller\DashboardPageController;
use Concrete\Core\File\Importer as FileImporter;
use \Concrete\Core\Page\Type\Type as CollectionType;

class CsvImport extends DashboardPageController 
{
  public $helpers = array('html', 'form');
  protected $results = array();
  protected $merchant_ids = null;
  // These aren't going to change
  protected $merchCategories = array(
    'Antiques & Art Galleries'  => 'Shopping',
    'Apparel'                   => 'Shopping',
    'Books, Cards & Stationery' => 'Shopping',
    'Churches'                  => 'Attractions',
    'Crafts, Toys & Hobbies'    => 'Shopping',
    'Florists'                  => 'Shopping',
    'Food Specialties'          => 'Shopping',
    'Gifts & Collectables'      => 'Shopping',
    'Home Furnishings'          => 'Shopping',
    'Jewelry'                   => 'Shopping',
    'Lodging'                   => 'Businesses',
    'Media'                     => 'Businesses',
    'Medical & Pharmacies'      => 'Shopping',
    'Music'                     => 'Shopping',
    'Printers'                  => 'Shopping',
    'Restaurants'               => 'Dining',
    'Shoes & Accessories'       => 'Shopping',
    'Spas, Salons & Fitness'    => 'Businesses',
    'Specialty Shops'           => 'Shopping',
    'Sporting Goods'            => 'Shopping',
    'Wedding'                   => 'Shopping'
  );

  public function view() 
  {
    $this->setFormData();
  }

  public function process()
  {
    if ($this->token->validate("import")) {
      $valn = Core::make("helper/validation/numbers");
      $fi = new FileImporter();
        
      $fID = $this->post('fID');
      if (!$valn->integer($fID)) {
         $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
      } else {
        $f = File::getByID($fID);
        if (!is_object($f)) {
          $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
        }
      }

      if (is_null($this->post('delimiter')))
        $this->error->add('You must select a delimter.');
      else 
        $delimiter = $this->getDelimiter($this->post('delimiter'));
      $enclosure = $this->getEnclosure($this->post('enclosure'));
        
      if (!$this->error->has()) {
        $fsr = $f->getFileResource();
        if (!$fsr->isFile()) {
          $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
        } else {
          $this->set('message', t('File Parsed Successfully'));
          $rows = $this->parse_file($fsr, $delimiter, $enclosure);
          $this->set('pending_rows', $rows);
          Config::save('merchant-import.last-import-id', $this->post('fID'));
          Config::save('merchant-import.last-import-delimiter', $this->post('delimiter'));
          Config::save('merchant-import.last-import-enclosure', $this->post('enclosure'));
          $this->setFormData();
        }
      }
    } else {
      $this->error->add($this->token->getErrorMessage());
    }
  }


  private function parse_file($fsr, $delimiter, $enclosure) 
  {
    $rows = $rows = array_filter(explode("\n", $fsr->read()));
    $header = null;
    $data = array();
    $this->loadMerchantIds();
    foreach ($rows as $row) {
      $row = str_getcsv($row, $delimiter, $enclosure);
      if(!$header)
        $header = $row;
      else {
        $row = $this->mapData(array_combine($header, $row));
        if (array_key_exists(trim($row['name']), $this->merchant_ids)) {
          $row['cID'] = $this->merchant_ids[$row['name']];
          $row['action'] = 'update';
        }
        else {
          $row['action'] = 'create';
        }
        $data[] = $row;
      }
    }
    return $data;
  }

  private function mapData($row)
  {
    $config = array(
      'name' => 'Business Name',
      'cDescription' => 'Tagline',
      'meta_keywords' => 'Keywords',
      'merchant_address' => array('Number','Direction','Street','Address 2','City','State','Zip'),
      'merchant_phone' => 'Phone',
      'merchant_website' => 'Website',
      'merchant_category' => 'Category',
      'merchant_dtca_member' => 'Member',
      'merchant_district' => 'District',
      'merchant_instagram_post_id' => 'Instagram Video',
      'merchant_google_places_id' => 'Google Places ID',
      'merchant_facebook' => 'Facebook Link',
      'merchant_twitter' => 'Twitter Link',
      'merchant_instagram' => 'Instagram Link'
    );
    // Translate the array into somehting where the keys match our attribute names
    $pageData = array();
    foreach ($config as $attr => $value) {
      if ($attr == 'merchant_address') {
        $address = array_values(array_filter(array_intersect_key($row, array_flip($value))));
        $address[count($address) - 3] .= ',';
        $pageData[$attr] = implode(" ", $address);
      }
      else {
        $pageData[$attr] = $row[$value];
      }
    }
    return $pageData;
  }

  private function setFormData() 
  {
    $this->set('fID', Config::get('merchant-import.last-import-id'));
    $this->set('delimiter', Config::get('merchant-import.last-import-delimiter'));
    $this->set('enclosure', Config::get('merchant-import.last-import-enclosure'));
  }

  private function getDelimiter($param) 
  {
    if ($param == 'comma')
      return ",";
    elseif ($param == 'tab') 
      return "\t";
    else
      $this->error->add('Invalid delimter selected.');
  }

  private function getEnclosure($param) 
  {
    if ($param == 'double-quoute')
      return '"';
    elseif ($param == 'single-quoute')
      return "'";
    else
      return null;
  }

  private function loadMerchantIds() 
  {
    $pages = new PageList();
    $merchant_type = CollectionType::getByHandle('merchant_page');
    if (is_object($merchant_type)) {
      $pages->filterByPageTypeID($merchant_type->getPageTypeID());
    }
    $page_ids = array();
    foreach ($pages->getResults() as $page) {
      $this->merchant_ids[$page->getCollectionName()] = $page->getCollectionID();
    }
  }
}