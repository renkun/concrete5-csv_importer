<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>

<?php if ($result['success']) { 
  $page = $result['page'];
?>
<div class="alert alert-success">
  <div><a href="<?php echo $page->getCollectionPath() ?>" target="_blank"><?php echo $page->getCollectionName() ?></a> <?php echo $action ?></div>
</div>

<?php } else { ?>
<div class="alert alert-danger">
  <div>Failed to import <?php echo $name ?>:</div>
  <ul>
  <?php foreach ($result['errors'] as $error) { ?>
    <li><?php echo $error ?></li>
  <?php } ?>
  <ul>
</div>
<?php } ?>