<?php  defined('C5_EXECUTE') or die("Access Denied.");
// Get file object if $fID is set
$f = null;  
if ($fID) {
  $f = File::getById($fID);
}
?>

<style type="text/css">
  .page-details h5 {
    margin-left: 15px;
  }
  .page-details .table {
    border: 1px solid #ddd;
    margin: 10px;
  }
  .progress-container {
    position: fixed; 
    top: 55px; 
    right: 340px; 
    width: 600px;
    padding: 30px;
    z-index: 900;
  }
  .progress-container #progress-update {
    float: left;
  }
</style>

<?php if ($pending_rows) { 
  $urls = array(
    'update' => URL::to('csv_importer/routes/update'),
    'create' => URL::to('csv_importer/routes/create')
  )
?>
<div class="progress-container alert alert-info" style="display: none;">
  <div id="progress-update">
    &nbsp;
  </div>
  <button type="button" class="close" data-dismiss="alert">×</button>
</div>
<div class="ccm-dashboard-content-form">
  <fieldset>
    <legend><?php  echo t('CSV Import'); ?></legend>
    <div class="form-group">
      <div class="col-xs-12">
        <div class="pull-left">
          <img src="/concrete/images/icons/filetypes/csv.png" class="img-responsive ccm-generic-thumbnail" style="height: 60px;">
        </div>
        <div class="pull-left" style="padding: 20px;"><?php echo $f->getFileName() ?></div>
      </div>
    </div>
    <div class="form-group">
      <legend style="font-size: 13px; padding-top: 30px;">
        <a href="javascript:void({});" id="deselect-all">Deselect All</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void({});" id="select-all">Select All</a>
      </legend>

    </div>
  </fieldset>
  <div class="row">
    <div class="col-xs-12">
      <div class="container-fluid">
      <?php 
        $count = 0;
        foreach ($pending_rows as $row) { 
          $id = 'row-'.$count++;
      ?>
        <div class="row">
          <div class="col-xs-12">
            <form class="page-form" id="<?php echo $id ?>" action="<?php echo $urls[$row['action']] ?>">
              <div class="alert alert-<?php echo ($row['action'] == 'create' ? 'danger' : 'warning') ?>">
                <input type="checkbox" class="include" name="include" value="1" checked />
                <strong><?php echo strtoupper($row['action']) ?>:</strong> <?php echo $row['name'] ?>
                <?php foreach ($row as $key => $value) {
                  echo $form->hidden($key, $value);
                } ?>
                <div class="page-details">
                  <a href="javascript:void({});" id="<?php echo $id ?>-open" onclick="$('#<?php echo $id ?>-close').show(); $('#<?php echo $id ?>-details').show(); $(this).hide();"><h5><i class="fa fa-plus-circle"></i> Show Details</h5></a>
                  <a href="javascript:void({});" id="<?php echo $id ?>-close" style="display: none;" onclick="$('#<?php echo $id ?>-open').show(); $('#<?php echo $id ?>-details').hide(); $(this).hide();"><h5><i class="fa fa-minus-circle"></i> Hide Details</h5></a>
                  <table class="table" id="<?php echo $id ?>-details" style="display: none;">
                    <tr><th>Attribute</th><th>Value</th></tr>
                    <?php foreach ($row as $key => $value) { ?>
                    <tr><td><?php echo $key ?></td><td><?php echo $value ?></td></tr>
                    <?php } ?>
                  </table>
                </div>
              </div>
            </form>
          </div>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="ccm-dashboard-form-actions-wrapper">
  <div class="ccm-dashboard-form-actions">
    <a href="<?php echo URL::to('/dashboard/csv_import')?>" class="btn pull-left btn-default"><?php echo t('Cancel')?></a>
    <button class="pull-right btn btn-success" id="run-import"><?php  echo t('Run')?></button>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#deselect-all').click(function(){
      $('.include').prop('checked', false);
    });
    $('#select-all').click(function(){
      $('.include').prop('checked', true);
    });
    $('#run-import').click(function(){
      $('.progress-container').show();
      $('.page-form').each(function() {
        if ($(this).find('input[name="include"]').prop('checked')) {
          var name = $(this).find('input[name="name"]').val();
          makeRequest($(this),name);
        }
      });

    });
  });

  function makeRequest($form,$name) {
    $.ajax({
      type: "POST",
      url: $form.attr('action'),
      data: $form.serialize(),
      async: true,
      success: function(response) {
        $form.html(response);
        $('#progress-update').html($name);
      },
      error: function (request, status, error) {
        $form.html('<div class="alert alert-denger">SERVER ERROR: '+request.responseText+'</div>');
      }
    });
  }
</script>

<?php } else { ?>

<form action="<?php  echo $this->action('process')?>" method="post" class="ccm-dashboard-content-form">
  <?php echo $this->controller->token->output('import')?>
  <fieldset>
    <legend><?php  echo t('CSV Import'); ?></legend>
    <div class="form-group">
      <div class="col-xs-12">
        <label for="fID" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('File should be in comma or tab delimited format. The selected import file will be remembered by this form until the next import.')?>"><?php echo t('Select File')?></label>
        <div class="controls">
          <?php  $al = Core::make('helper/concrete/asset_library'); ?>
          <?php  echo $al->text('fID', 'fID', 'Select File', $f); ?>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-3">
        <label for="delimiter" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('Delimiter is the character used to separate fields in the input file.') ?>"><?php echo t('Delimiter') ?></label>
        <select class="form-control" name="delimiter">
          <option value="comma"<?php if ($delimiter == 'comma') echo ' selected' ?>>Comma</option>
          <option value="tab"<?php if ($delimiter == 'tab') echo ' selected' ?>>Tab</option>
        </select>
      </div>
      <div class="col-xs-3">
        <label for="enclosure" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('Enclosure is the character used to wrap the values in the file. Enclosure is optional and will likely be present in files where the data may include the delimiter character. For example, if the data in a comma separated file could possibly include commas then a delimiter will be used.') ?>"><?php echo t('Enclosure') ?></label>
        <select class="form-control" name="enclosure">
          <option value="none"<?php if ($enclosure == 'none') echo ' selected' ?>>None</option>
          <option value="double-quote"<?php if ($enclosure == 'double-quote') echo ' selected' ?>>"</option>
          <option value="single-quote"<?php if ($enclosure == 'single-quote') echo ' selected' ?>>'</option>
        </select>
      </div>
    </div>
  </fieldset>

  <fieldset>
    <legend>Column Mapping</legend>
    <div class="alert alert-info">
      The input file must have a header row that identifies each column. The valid column names are listed in 
      the table below on the left. The page attributes they map to are listed in the right column.
    </div>
    <table class="table">
      <tr>
        <th>Column Name</th>
        <th>Page Attribute</th>
      </tr>
      <tr><td>Business Name</td><td>name</td></tr>
      <tr><td>Tagline</td><td>cDescription</td></tr>
      <tr><td>Keywords</td><td>meta_keywords</td></tr>
      <tr><td>Number<br/>Direction<br/>StreetAddress 2<br/>City<br/>State<br/>Zip</td><td>merchant_address</td></tr>
      <tr><td>Phone</td><td>merchant_phone</td></tr>
      <tr><td>Website</td><td>merchant_website</td></tr>
      <tr><td>Category</td><td>merchant_category</td></tr>
      <tr><td>Member</td><td>merchant_dda_member</td></tr>
      <tr><td>District</td><td>merchant_district</td></tr>
      <tr><td>Instagram Video</td><td>merchant_instagram_post_id</td></tr>
      <tr><td>Google Places ID</td><td>merchant_google_places_id</td></tr>
      <tr><td>Facebook Link</td><td>merchant_facebook</td></tr>
      <tr><td>Twitter Link</td><td>merchant_twitter</td></tr>
      <tr><td>Instagram Link</td><td>merchant_instagram</td></tr>
    </table>
  </fieldset>

  <div class="ccm-dashboard-form-actions-wrapper">
    <div class="ccm-dashboard-form-actions">
      <button class="pull-right btn btn-success" type="submit"><?php  echo t('Import')?></button>
    </div>
  </div>
</form>

<?php } ?>
