<?php 
namespace Concrete\Package\CsvImporter;

use Package;
use Route;
use \Concrete\Core\Page\Single as SinglePage;

class Controller extends Package
{
  protected $pkgHandle = 'csv_importer';
  protected $appVersionRequired = '5.7.3';
  protected $pkgVersion = '0.0.1';
    protected $pkgAutoloaderRegistries = array(
    'src/Import' => '\Import'
  );
  
  public function getPackageName()
  {
    return t('Merchant Import');
  }
  
  public function getPackageDescription()
  {
    return t('Create and update pages using a CSV file.');
  }

  public function on_start()
  {
    $this->registerRoutes();
  }

  public function install()
  {
    $pkg = parent::install();
    $this->installOrUpgrade($pkg);
  }

  public function upgrade()
  {
    $pkg = Package::getByHandle($this->pkgHandle);
    $this->installOrUpgrade($pkg);
    parent::upgrade();
  }

  public function installOrUpgrade($pkg)
  {
    $this->getOrAddSinglePage($pkg, 'dashboard/csv_import', $this->getPackageName());
  }

  private function getOrAddSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
    $sp = SinglePage::add($cPath, $pkg);
    if (is_null($sp)) {
      $sp = Page::getByPath($cPath);
    } else {
      $data = array();
      if (!empty($cName)) {
        $data['cName'] = $cName;
      }
      if (!empty($cDescription)) {
        $data['cDescription'] = $cDescription;
      }
      if (!empty($data)) {
        $sp->update($data);
      }
    }
    return $sp;
  }

  public function registerRoutes()
  {
    Route::register('/csv_importer/routes/update', '\Concrete\Package\CsvImporter\Controller\Ajax\MerchantPage\Import::update');
    Route::register('/csv_importer/routes/create', '\Concrete\Package\CsvImporter\Controller\Ajax\MerchantPage\Import::create');
  }
}